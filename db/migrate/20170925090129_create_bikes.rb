class CreateBikes < ActiveRecord::Migration[5.1]
  def change
    create_table :bikes do |t|
      t.string :name
      t.integer :year
      t.float :value

      t.timestamps
    end
  end
end
