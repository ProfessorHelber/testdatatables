class CreateCars < ActiveRecord::Migration[5.1]
  def change
    create_table :cars do |t|
      t.string :name
      t.string :plate
      t.integer :year
      t.float :value

      t.timestamps
    end
  end
end
