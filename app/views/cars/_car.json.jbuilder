json.extract! car, :id, :name, :plate, :year, :value, :created_at, :updated_at
json.url car_url(car, format: :json)
