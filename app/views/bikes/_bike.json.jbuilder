json.extract! bike, :id, :name, :year, :value, :created_at, :updated_at
json.url bike_url(bike, format: :json)
